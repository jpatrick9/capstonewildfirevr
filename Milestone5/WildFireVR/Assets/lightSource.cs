﻿/*
name: Joshua Patrick, Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class lightSource : MonoBehaviour {
    public Light lt;
    bool reverse = false;
    int ticks = 0;
    int degrees = 0;
    bool fadeColor = false;
    public Color colorA = Color.red;
    public Color colorB = Color.blue;
    Color[] colors = new Color[20];
    // Use this for initialization
    void Start()
    {
        lt = GetComponent<Light>();
    }
    // Update is called once per frame
    void Update()
    {
        float t = Mathf.PingPong(Time.time, 1.0f);
        lt.color = Color.Lerp(colorA, colorB, 0.01f);
        if (ticks <= 100)
        {
            degrees++;
            fadeColor = !fadeColor;
        }
        if ((degrees % 10) == 0)
        {
           colors[1] = colorA;
        }
        if (reverse == false)
        {
            transform.Rotate(Time.deltaTime / 2, 0, 0);
            ticks += 1;
            if (ticks >= 18000) // 60fps * 300sec = 18000 incrementations from night light to day light
            {
                reverse = !(reverse);
            }
        }
        else
        {
            transform.Rotate(-(Time.deltaTime / 2), 0, 0);
            ticks -= 1;
            if (ticks <= 1)
            {
                reverse = !(reverse);
            }
        }
    }
}