﻿/*
name: Alejandro Ruvalcaba, Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class tpbackpack : MonoBehaviour
{
	public GameObject lvl;
	public GameObject player;
	public float floor;
	public Vector3 pos;
	private bool lvlSelected = true;
	private bool outsidePlayArea = false;
	void Start () 
	{
		
		pos = transform.position;

	}
	void Update () 
	{
		if (lvl.GetComponent<Manager> ().levelChosen == 0) {
			lvlSelected = false;
		} else {
			lvlSelected = true;
		}
	}
	void OnCollisionEnter(Collision collision)
	{
		
			if (collision.collider.tag == "outofbound") {
					transform.position = pos;
			} 
		else if (collision.collider.tag == "NonCampsite" && lvlSelected == true){
				transform.position = pos;
			}
			else if (collision.collider.tag == "level"){
				pos= transform.position;

		}
	}
	void OnTriggerExit(Collider collision)
	{
		if (outsidePlayArea &&  !lvlSelected) {
			if (collision.tag == "Player") {
				outsidePlayArea = true;
	
			}
		}
	}
}
