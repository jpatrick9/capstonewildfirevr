﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rockCounter : MonoBehaviour {
	public bool inDirt = false;
	public bool inRock = false;
	public GameObject lvl;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter(Collider collision){
		if (collision.tag == "bad") {
			inDirt = true;
			lvl.GetComponent<Manager> ().wo = lvl.GetComponent<Manager> ().wo + 1;
		}
		if (collision.tag == "good") {
			inRock = true;
			lvl.GetComponent<Manager> ().wo = lvl.GetComponent<Manager> ().wo + 1;
		}
	}
	void OnTriggerExit(Collider collision) {
		if (collision.tag == "bad") {
			inDirt = false;
		}
		if (collision.tag == "good") {
			inRock = false;
		}
	}
}
