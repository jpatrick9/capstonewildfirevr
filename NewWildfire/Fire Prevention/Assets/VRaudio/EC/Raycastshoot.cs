﻿/*
name: Joshua Patrick
course: CST306
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycastshoot : MonoBehaviour {
	public ParticleSystem boom;
	public int gunDamage = 1;
	public float fireRate = .25f;
	public float weaponRange = 50f;
	public float hitForce = 200f;
	public Transform gunEnd;


	private Camera fpsCam;
	private WaitForSeconds shotDuration = new WaitForSeconds (.07f);
	private AudioSource gunAudio;
	private LineRenderer laserline;
	private float nextfire;
	public Animator shoot;

	void Start () {
		laserline = GetComponent<LineRenderer> ();
		gunAudio = GetComponent <AudioSource> ();
		fpsCam = GetComponentInParent<Camera>();
		shoot = GetComponentInChildren<Animator> ();
	}
	

	void Update () {

		if (Input.GetButtonDown ("Fire1") && Time.time > nextfire) {
			

			nextfire = Time.time + fireRate;
			StartCoroutine (ShotEffect ());

			shoot.SetTrigger ("shoots");

			Vector3 rayOrigin = fpsCam.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, 0));
			RaycastHit hit;


			laserline.SetPosition (0, gunEnd.position);

			if (Physics.Raycast (rayOrigin, fpsCam.transform.forward, out hit, weaponRange)) {
				laserline.SetPosition (1, hit.point);


				if (hit.transform.root.gameObject.name == "NPC" || hit.transform.root.gameObject.name == "NPC(Clone)") {

					hit.transform.root.gameObject.GetComponent<Animator>().enabled = false;

				}



			} else {
				laserline.SetPosition (1, rayOrigin + (fpsCam.transform.forward * weaponRange));
			}
		}





		
	}
	private IEnumerator ShotEffect(){

		gunAudio.Play ();
		boom.Play ();
		laserline.enabled = true;
		yield return shotDuration;
		laserline.enabled = false; 
	}


}
