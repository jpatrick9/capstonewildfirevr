﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class removeshovel : MonoBehaviour {
	public Rigidbody backpack;
	public Rigidbody shovel; 
	public bool connect = false;

	// Use this for initialization
	void Start () {
		shovel.isKinematic = true;

	}
	
	// Update is called once per frame
	void Update () {
		if( connect == true && backpack.transform.parent != null){
			backpack.gameObject.transform.parent = null;
			shovel.isKinematic = false;
		}
		//else if (active == true && backpack.transform.parent == null)
			//backpack.gameObject.transform.parent = NULL;
	}
}
