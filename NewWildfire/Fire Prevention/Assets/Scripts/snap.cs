﻿/*
name: Brieg Oudeacoumar
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snap : MonoBehaviour {
	public GameObject backpack;

	public GameObject shovel;
	public FixedJoint shovelJoint;

	public GameObject match;
	public FixedJoint matchJoint;

	public GameObject bucket;
	public FixedJoint bucketJoint;

	public GameObject tablet;
	public FixedJoint tabletJoint;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider collision)
	{
		if(collision.tag == "shovel"||collision.tag == "bucket"||collision.tag == "match"||collision.tag == "tablet")
		{
			if(collision.tag == "shovel")
			{
				shovelJoint.connectedBody = shovel.GetComponent<Rigidbody>();
				shovel.GetComponent<tpShovel>().onBackpack = true;
			}
			if (collision.tag == "bucket")
			{
				bucketJoint.connectedBody = bucket.GetComponent<Rigidbody>();
				bucket.GetComponent<tpbucket1>().onBackpack = true;
			}
			if (collision.tag == "match")
			{
				matchJoint.connectedBody = match.GetComponent<Rigidbody>();
				match.GetComponent<tpMatch>().onBackpack = true;
			}
			if (collision.tag == "tablet")
			{
				tabletJoint.connectedBody = tablet.GetComponent<Rigidbody>();
				//tablet.GetComponent<tpTablet>().onBackpack = true;
			}
		}
	}
}